$(function() {
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Janurary", "February", "March", "April", "May", "June", "July", "August", "September","October","November","December"],
            datasets: [{
                label: 'Book trend',
                data: [82, 99, 3, 15, 82, 63, 36,17,25,32,65,43],
                backgroundColor: ['rgba(255, 99, 132, 0.2)'],
                borderColor: ['rgba(255,99,132,1)'],
                borderWidth: 1
            },{
                label: '9t6 users',
                data: [23, 59, 86, 75, 42, 33, 6,87,23,42,55,93],
                backgroundColor: ['rgba(54, 162, 235, 0.2)'],
                borderColor: ['rgba(54, 162, 235, 1)'],
                borderWidth: 1 
            },{
                label: 'Reviews',
                data: [12, 59, 43, 55, 22, 33, 86,47,55,92,35,100],
                backgroundColor: ['rgba(255, 159, 64, 0.2)'],
                borderColor: ['rgba(255, 159, 64, 1)'],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
$(".rating").rate();
    var options = {
        max_value: 5,
        step_size: 0.5,
        initial_value: 1,
        selected_symbol_type: 'utf8_star', // Must be a key from symbols
        cursor: 'default',
        readonly: true,
    }
    $(".rating").rate(options);
});